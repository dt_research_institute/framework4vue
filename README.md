# Framework4Vue

## 简介

这是一个为后端开发人员服务的项目

基于Vue前端框架及各种Ui框架,构造最简单的后台开发脚手架，后端开发工程师可根据自己喜好,选择不同的版本,并在此技术上进行二次开发,运用到自己的项目中

**目标只有一个：简单、简单、简单~!!!**

> 当前前提是你得会Vue,不过不会也没关系,这些简单的脚手架会带来慢慢熟悉Vue的特性,久而久之你就越来越熟了，依葫芦画瓢(其实我也是照着画的...)，还能增强你的自信心.

主要封装的功能项包括：

- 菜单项
- 滚动下拉框
- 自动切换等
- 内页

除了以上的功能外,本项目没有其他的东西了，开发者自行填充内容页,达到快速开发项目的目的.

## 已开发完成

- 基于Vue+Ant Design 简单的后台(simple-back)

  ![](images/simple-back.gif)

- 基于Vue+Ant Design多Tab的后台(simple-back-tab)

  ![](images/simple-back-tab.gif)

- 基于Vue+Ant Design多Tab的后台的黑色主题(simple-back-tab-black)

  ![](images/simple-back-tab-black.gif)

